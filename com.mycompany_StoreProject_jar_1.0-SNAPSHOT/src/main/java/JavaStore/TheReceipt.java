/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JavaStore;

import Model.Customer;
import Model.Product;
import Model.Receipt;
import Model.User;

/**
 *
 * @author lovel
 */
public class TheReceipt {

    public static void main(String[] args) {
        Product p1 = new Product(1, "Chayen", 30);
        Product p2 = new Product(2, "Americano", 40);
        User seller = new User(1, "Lisa Manoban", "123456789", "password");
        Customer customer = new Customer(1, "Rose", "9999999999");
        Receipt receipt = new Receipt(seller, customer);
        receipt.addReceiptDetail(p1, 1);
        receipt.addReceiptDetail(p2, 1);
        System.out.println(receipt);
        receipt.deleteReceiptDetail(0);
        System.out.println(receipt);
        receipt.addReceiptDetail(p1, 3);
        System.out.println(receipt);
        receipt.addReceiptDetail(p1, 3);
        System.out.println(receipt);
    }

}
