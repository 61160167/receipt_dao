/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JavaStore;

import database.Database;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lovel
 */
public class Testdeleteproduct {

    public static void main(String[] args) throws SQLException {
        Connection con = null;
        Database db = Database.getIntstance();
        con = db.getConnection();

        try {
            String sql = "DELETE FROM product WHERE id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, 5);
            int row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            Logger.getLogger(Testselecproduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }
}
