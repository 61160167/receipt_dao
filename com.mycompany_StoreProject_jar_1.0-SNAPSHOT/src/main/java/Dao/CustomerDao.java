/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import Model.Customer;

/**
 *
 * @author lovel
 */
public class CustomerDao {

    public int add(Customer object) throws SQLException {
        Connection conn = null;
        String dbPath = "./db/store.db";
        Database db = Database.getIntstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO customer(name,tel)VALUES (?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return id;
    }

    public ArrayList<Customer> getAll() throws SQLException {
        Connection conn = null;
        String dbPath = "./db/store.db";
        Database db = Database.getIntstance();
        conn = db.getConnection();
        ArrayList<Customer> list = new ArrayList<>();
        //select
        try {
            String sql = "SELECT id, name, tel FROM customer";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                Customer customer = new Customer(id, name, tel);
                list.add(customer);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return list;
    }

    public Customer get(int id) throws SQLException {
        Connection conn = null;
        String dbPath = "./db/store.db";
        Database db = Database.getIntstance();
        conn = db.getConnection();

        //select
        try {
            String sql = "SELECT id, name, tel FROM customer WHERE id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int pid = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                Customer customer = new Customer(pid, name, tel);
                return customer;
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return null;
    }

    public int delete(int id) throws SQLException {
        Connection conn = null;
        String dbPath = "./db/store.db";
        Database db = Database.getIntstance();
        conn = db.getConnection();

        int row = 0;
        try {
            String sql = "DELETE FROM customer WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    public int update(Customer object) throws SQLException {
        Connection conn = null;
        String dbPath = "./db/store.db";
        Database db = Database.getIntstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE customer SET name = ?,tel = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setInt(3, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    public static void main(String[] args) throws SQLException {
        CustomerDao dao = new CustomerDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Customer(-1, "cher cher", "0863000000"));
        System.out.println(id);
        Customer lastCustomer = dao.get(id);
        System.out.println("last customer : " + lastCustomer);
        lastCustomer.setName("NumNum");
        dao.update(lastCustomer);
        Customer updateCustomer = dao.get(id);
        System.out.println("update cutomer : " + updateCustomer);
        dao.delete(id);
        System.out.println("delete customer : " + dao.get(id));
    }
}
